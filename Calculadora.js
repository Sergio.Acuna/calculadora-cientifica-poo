class Calculadora{

    variable_1;
    variable_2;
    resultado;

    constructor(variable_1, variable_2, resultado){
           
        this.resultado = resultado; 
        this.variable_1 = variable_1;
        this.variable_2 = variable_2;
        

    }

    sumar(){
        this.resultado = this.variable_1 + this.variable_2; 
        return this.resultado;
    }
    restar(){
        this.resultado = this.variable_1 - this.variable_2; 
        return this.resultado;
    }
    multiplicar(){
        this.resultado = this.variable_1 * this.variable_2; 
        return this.resultado;
    }
    dividir(){
        this.resultado = this.variable_1 / this.variable_2; 
        return this.resultado;
    }
    modulo(){
        this.resultado = this.variable_1 % this.variable_2; 
        return this.resultado;
    }
};

class CalculadoraCientifica extends Calculadora{


    calcularseno(){
        this.resultado = Math.sin(this.variable_1)*(180/(Math.PI));
        return this.resultado;
    }
    calcularcoseno(){
        this.resultado = Math.cos(this.variable_1)*(180/(Math.PI));
        return this.resultado;
    }
    calculartangente(){
        this.resultado = Math.tan(this.variable_1)*(180/(Math.PI));
        return this.resultado;
    }  
      
};

class CalculadoraConversora extends CalculadoraCientifica{


    gradosradianes(){
        this.resultado = ((Math.PI)/180) * this.variable_1;
        return this.resultado;
    }
    radianesgrados(){
        this.resultado = (180/(Math.PI)) * this.variable_1; 
        return this.resultado;
    }
    kelvinacentigrados(){
        this.resultado = this.variable_1 - (273.15);
        return this.resultado;
    }
       
};

   let miCalculadora = new CalculadoraConversora();
    
    miCalculadora.variable_1 = 40;
    miCalculadora.variable_2 = 2;
    
    console.log(miCalculadora.sumar());
    console.log(miCalculadora.restar());
    console.log(miCalculadora.multiplicar());
    console.log(miCalculadora.dividir());
    console.log(miCalculadora.modulo());
    console.log(miCalculadora.calcularseno());
    console.log(miCalculadora.calcularcoseno());
    console.log(miCalculadora.calculartangente());
    console.log(miCalculadora.gradosradianes());
    console.log(miCalculadora.radianesgrados());
    console.log(miCalculadora.kelvinacentigrados());
    